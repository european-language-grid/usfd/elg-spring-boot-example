#
# File for building this example into a Docker image suitable to be run on ELG
#

# This example uses Java 8, but you can substitute 9, 10, 11, 12 or 13 if required
FROM openjdk:8-jdk-slim

# Use https://github.com/krallin/tini so the Java process is not running as PID
# 1 in the container, and create an unprivileged user account so it is not
# running as root un-necessarily
ADD https://github.com/krallin/tini/releases/download/v0.19.0/tini /sbin/tini
RUN addgroup --gid 1001 "elg" && \
      adduser --disabled-password --gecos "ELG User,,," \
      --home /elg --ingroup elg --no-create-home --uid 1001 elg && \
      chmod +x /sbin/tini

# Put the Spring Boot runnable JAR file into the image
COPY --chown=elg:elg build/libs/elg-spring-boot-example-0.0.1-SNAPSHOT.jar /elg/

# Run as the unprivileged user
USER elg:elg

WORKDIR /elg

# We split the ENTRYPOINT and CMD so it's easier to override the active
# profiles when testing locally
ENTRYPOINT ["/sbin/tini", "-e", "143", "--", "java", "-jar", "/elg/elg-spring-boot-example-0.0.1-SNAPSHOT.jar"]
CMD ["--spring.profiles.active=elg-simple"]
