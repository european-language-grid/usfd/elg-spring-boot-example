package com.example.elg.handler;

import eu.elg.handler.ElgHandler;
import eu.elg.handler.ElgMessageHandler;
import eu.elg.model.requests.AudioRequest;
import eu.elg.model.responses.ClassificationClass;
import eu.elg.model.responses.ClassificationResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Profile;
import org.springframework.core.io.buffer.DataBuffer;
import org.springframework.stereotype.Component;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;


@Component
@ElgHandler
public class AudioLengthHandler {

  private static final Logger log = LoggerFactory.getLogger(AudioLengthHandler.class);

  /**
   * Trivial handler for audio messages that counts up the total size of the
   * data and returns that as if it were a classification score.
   */
  @ElgMessageHandler
  public Mono<ClassificationResponse> audioLength(AudioRequest request, Flux<DataBuffer> content) {
    return content.reduce(0L, (acc, buf) -> {
      return acc + buf.readableByteCount();
    })
    .map((totalSize) -> new ClassificationResponse().withClasses(
          new ClassificationClass().withClassName("length").withScore(totalSize)));
  }
}
