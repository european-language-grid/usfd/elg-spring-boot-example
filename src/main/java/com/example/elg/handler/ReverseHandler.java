package com.example.elg.handler;

import eu.elg.handler.ElgHandler;
import eu.elg.handler.ElgMessageHandler;
import eu.elg.handler.HandlerException;
import eu.elg.model.StandardMessages;
import eu.elg.model.requests.TextRequest;
import eu.elg.model.responses.TextsResponse.Text;
import eu.elg.model.responses.TextsResponse;

import org.apache.commons.lang3.ArrayUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Component;

/**
 * Very simple example message handler that expects a text request and returns
 * a texts response with various reversals of that text.
 */
@Profile("elg-simple")
@Component
@ElgHandler
public class ReverseHandler {

  @ElgMessageHandler
  public TextsResponse reverseText(TextRequest request) throws HandlerException {
    if(request.getContent() == null) {
      throw new HandlerException(StandardMessages.elgServiceInternalError("No content"));
    }
    Text simpleReverse = new Text().withContent(
        new StringBuilder(request.getContent()).reverse().toString());
    String[] words = request.getContent().trim().split("(?U)\\s+");
    ArrayUtils.reverse(words);
    Text wordsReverse = new Text().withContent(
        StringUtils.join(words, ' '));

    return new TextsResponse().withTexts(simpleReverse, wordsReverse);
  }
}
