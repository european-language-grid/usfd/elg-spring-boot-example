package com.example.elg.handler;

import eu.elg.handler.ElgHandler;
import eu.elg.handler.ElgMessageHandler;
import eu.elg.handler.HandlerException;
import eu.elg.handler.ProgressReporter;
import eu.elg.model.StandardMessages;
import eu.elg.model.StatusMessage;
import eu.elg.model.requests.TextRequest;
import eu.elg.model.responses.TextsResponse.Text;
import eu.elg.model.responses.TextsResponse;

import org.apache.commons.lang3.ArrayUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Component;

/**
 * More complex handler that issues partial progress reports as it does its
 * work.
 */
@Profile("elg-progress")
@Component
@ElgHandler
public class ReverseHandlerWithProgress {

  @ElgMessageHandler
  public TextsResponse reverseText(TextRequest request,
      ProgressReporter progress) throws HandlerException {
    if(request.getContent() == null) {
      throw new HandlerException(StandardMessages.elgServiceInternalError("No content"));
    }
    Text simpleReverse = new Text().withContent(
        new StringBuilder(request.getContent()).reverse().toString());

    progress.reportProgress(50.0d, new StatusMessage()
        .withCode("com.example.progress.doneSimple").withText("Completed simple reverse"));

    try {
      // sleep for more than 15 seconds, to demonstrate the fact that the most
      // recent progress report will be re-issued automatically in event-stream
      // mode to keep the connection alive.
      Thread.sleep(25000);
    } catch(Exception e) {}

    String[] words = request.getContent().trim().split("(?U)\\s+");
    ArrayUtils.reverse(words);
    Text wordsReverse = new Text().withContent(
        StringUtils.join(words, ' '));

    progress.reportProgress(100.0d, new StatusMessage()
        .withCode("com.example.progress.doneWords").withText("Completed words reverse"));

    return new TextsResponse().withTexts(simpleReverse, wordsReverse);
  }
}
